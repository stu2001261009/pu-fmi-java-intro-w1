public class HelloWorld {

	public static void main(String[] args) {
		long number = 2001261009;
        String name = "Petar Ivanov";
        System.out.println(name + " - " + number);
        int sword = 15, armor = 10, pistol = 1, tank = 1, barrel = 1;
        double sack = 169.77;
        String[] serial_number = {"S5", "A5", "P6", "T4", "B6","S4"};
        System.out.println("Serial numbers:");
        for(int i = 0; i < serial_number.length; i++)
        {
            if (i+1 == serial_number.length)
            System.out.println(serial_number[i]);
            else
            System.out.print(serial_number[i] + " ");
        }
        String[] quality = {"Metal", "Aluminium", "Gas", "Wooden", "Plastic", "Leather"};
        System.out.println("Qualities:");
        for(int i = 0; i < quality.length; i++)
        {
            if (i+1 == quality.length)
            System.out.println(quality[i]);
            else
            System.out.print(quality[i] + " ");
        }
        String[] objects = {"Sowrds", "Armors", "Pistol", "Tank", "Barrel", "Sack"};
        System.out.println("Objects:");
        for(int i = 0; i < objects.length; i++)
        {
            if (i+1 == objects.length)
            System.out.println(objects[i]);
            else
            System.out.print(objects[i] + " ");
        }

	}

}
